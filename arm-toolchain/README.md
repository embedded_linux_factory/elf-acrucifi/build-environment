# arm-toolchain docker image

`arm-toolchain` is an example of docker image based on build-environment which
provides all tools to build and debug application using `arm-none-eabi` toolchain.

This `arm-toolchain` docker image provides all tools to build and
debug application using `arm-none-eabi` toolchain.
It is can be taken as example to build an image based
on `build-environment` docker image.

## How-to build the docker image

We assume here you already have built the `build-environment` image,
if not please refer to [build-environment](../build-environment/).

To build the image, use the Makefile:

```shell
$ docker images --filter reference=build-environment
REPOSITORY          TAG       IMAGE ID       CREATED          SIZE
build-environment   latest    abcdef123456   12 minutes ago   123MB

$ make
...
```

## How-to launch the docker image

The `arm-toolchain` docker image can be started using `arm-toolchain` script:

```shell
$ ./arm-toolchain [... "docker run additional arguments"] \
> [-c ... "command executed in container"]
...
```
