# Build environment docker images

This repository provides a base docker image useful to build compile
and debug environment docker images for developer and CI.

## build-environment

This is the base docker image and associated tools,
see [build-environment](build-environment/) for more details.

## arm-toolchain

`arm-toolchain` is an example of docker image based on `build-environment`
which provides all tools to build and debug application
using `arm-none-eabi` toolchain,
see [arm-toolchain](arm-toolchain/) for more details.

## yocto-env

`yocto-env` is a docker image based on `build-environment`
which provides all tools to build a yocto project,
see [yocto-env](yocto-env/) for more details.

## How-to build the docker images

To build the image, use the Makefile:

```shell
$ make all
...
```

## How-to contribute

To contribute to this project, please ensure that the following tools are installed:

* gitlint
* pre-commit
* shfmt
* shellcheck

You can automatically check and install the tools using the following command:

```shell
$ ./dev-env-setup.sh
Start of dev environment setup
Checking tools...
* pip... OK
* shfmt... OK
* shellcheck... Installed
* pre-commit... OK
* gitlint... OK

Checking hooks...
* pre-commit... Installed
* commit-msg... OK

Dev environment is ready!
```
