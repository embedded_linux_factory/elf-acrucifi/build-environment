# yocto-env docker image

`yocto-env` is a docker image based on `build-environment`
which provides all tools to build a yocto project.

This `yocto-env` docker image contains all tools required by
[Yocto Project Quick Build](https://docs.yoctoproject.org/dev/brief-yoctoprojectqs/index.html#build-host-packages)
manual in addition with `git-repo`.

## How-to build the docker image

We assume here you already have built the `build-environment` image,
if not please refer to [build-environment](../build-environment/).

To build the image, use the Makefile:

```shell
$ docker images --filter reference=build-environment
REPOSITORY          TAG       IMAGE ID       CREATED          SIZE
build-environment   latest    abcdef123456   12 minutes ago   123MB

$ make
...
```

## How-to launch the docker image

The `yocto-env` docker image can be started using `yocto-env` script:

```shell
$ ./yocto-env [... "docker run additional arguments"] \
> [-c ... "command executed in container"]
...
```
