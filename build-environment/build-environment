#!/usr/bin/env bash

set -e

SOURCE=${BASH_SOURCE[0]}
while [ -L "$SOURCE" ]; do
	# resolve $SOURCE until the file is no longer a symlink
	TARGET=$(readlink "$SOURCE")
	if [[ $TARGET == /* ]]; then
		SOURCE=$TARGET
	else
		DIR=$(dirname "$SOURCE")
		# if $SOURCE was a relative symlink, we need to resolve it
		# relative to the path where the symlink file was located
		SOURCE=$DIR/$TARGET
	fi
done

SCRIPT_DIR_PATH=$(dirname "$SOURCE")
SCRIPT_NAME=$(basename "$0")
DOCKER_IMAGE=build-environment:latest
RUN_DOCKER=$(realpath "$SCRIPT_DIR_PATH/run-docker.py")
ARGS=()

while [ $# -gt 0 ]; do
	case $1 in
	-h | --help)
		echo -e "Run the $DOCKER_IMAGE docker image\n"
		$RUN_DOCKER --help |
			sed -n '/^usage:/,/^$/p;/arguments\?:$/,$p' |
			sed "s/$(basename "$RUN_DOCKER")/$SCRIPT_NAME/" |
			sed "s/-I image //" |
			sed '/--image/,+1d'
		exit 0
		;;
	*)
		ARGS=(
			"${ARGS[@]}"
			"$1"
		)
		;;
	esac
	shift
done

$RUN_DOCKER --image $DOCKER_IMAGE "${ARGS[@]}"
