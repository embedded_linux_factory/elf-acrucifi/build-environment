
WORKSPACE_PATH := $(realpath $(dir $(abspath $(lastword $(MAKEFILE_LIST)))))

build-environment:
	@make -C $(WORKSPACE_PATH)/build-environment build-environment

arm-toolchain: build-environment
	@make -C $(WORKSPACE_PATH)/arm-toolchain arm-toolchain

yocto-env: build-environment
	@make -C $(WORKSPACE_PATH)/yocto-env yocto-env

all: build-environment arm-toolchain yocto-env

.PHONY : all build-environment arm-toolchain yocto-env
.DEFAULT_GOAL := all
